# Dstat from VCF

This python script computes the Patterson's D, also known as ABBA-BABA directly from a VCF file

Usage:
```bash
python3 Dstat.py <options> <-p "OUT,Test,X,Y"> <file.vcf/.gz>
```

Options:

    -n : Number of blocks used for the Jacknife resampling (Default 20)

    , alternatively,

    -s : Size in BP of each block used for the Jacknife resampling
    --print-sites : Reports every site with a ABBA / BABA pattern (to stderr)
