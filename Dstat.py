#!/usr/bin/python3

import vcfpytools
from scipy.stats import sem
from sys import argv, stderr, exit

help_message = '''
Usage:
python3 Dstat.py <options> <-p "OUT,Test,X,Y"> <file.vcf/.gz>

Options:
    -n <int> : Number of blocks used for the Jacknife resampling (Default 20)
    , alternatively,
    -s <int> : Size in BP of each block used for the Jacknife resampling

    --print-sites : Reports every site with a ABBA / BABA pattern (to stderr)
'''

if '-h' in argv or len(argv) < 2:
    print(help_message)
    exit()

# Parsing options
VCF = argv[-1]
try:
    chrlengths = vcfpytools.get_chr_lengths(VCF)
except:
    print('\nERROR: No VCF file was provided')
    print(help_message)
    exit()

## Define number of blocks or blocksize for Jacknife
if '-s' in argv:
    blocksize = int(argv[argv.index('-s') + 1])
else:
    if '-n' in argv:
        num_blocks = int(argv[argv.index('-n') + 1])
    else:
        num_blocks = 20 # Default is no option is provided; it might change due to the length per chromosome
    blocksize = int(sum(chrlengths.values()) / num_blocks)

## Print ABBA BABA sites
printsites = False
if '--print-sites' in argv:
    printsites = True

## 4-taxon configuration
try:
    config = argv[argv.index('-p') + 1] # Format: OUT,TEST,X,Y
except ValueError:
    print('\nERROR: You must provide a configuration: "-p OUT,TEST,X,Y"')
    print(help_message)
    exit()

# Main function
def ABBA_or_BABA(genotypes):
    if genotypes.count('0') == 0 or genotypes.count('1') == 0:
        return(None)
    elif genotypes == ['0','1','1','0'] or genotypes == ['1','0','0','1']:
        return('ABBA')
    elif genotypes == ['0','1','0','1'] or genotypes == ['1','0','1','0']:
        return('BABA')

# Parsing VCF file with the main function
if printsites == True:
    print('CHR\tPOS\tTYPE', file = stderr)
out = {i:[] for i in (chrlengths.keys())}
for record in vcfpytools.get_genotypes_hap(VCF, samples = config.split(','), binary = True):
    if '.' in record['Genotypes']:
        continue
    site = ABBA_or_BABA(record['Genotypes'])
    if site != None:
        out[record['Position'][0]].append((int(record['Position'][1]),site))
        if printsites == True:
            print(record['Position'][0], record['Position'][1], site, sep = '\t', file = stderr)

# Assigning ABBA BABA counts to blocks
glob = []
for chr in chrlengths.keys():
    for block in list(range(1,chrlengths[chr],blocksize)):
        ABBA = 0
        BABA = 0
        for i in out[chr]:
            if i[0] >= block and i[0] < (block + blocksize):
                if i[1] == 'ABBA':
                    ABBA += 1
                elif i[1] == 'BABA':
                    BABA += 1
        glob.append((ABBA, BABA))

# Calculation of D statistic
def Dval(listABBABABA):
    ABBA = sum([i[0] for i in listABBABABA])
    BABA = sum([i[1] for i in listABBABABA])
    D = (ABBA - BABA) / (ABBA + BABA)
    return(D)

# Jacknife
pseudovals = []
for block in range(len(glob)):
    ite = [i for i in glob]
    ite.pop(block)
    pseudovals.append(Dval(ite))

# Results
N_ABBA = sum([i[0] for i in glob])
N_BABA = sum([i[1] for i in glob])
D = Dval(glob)
Dcorr = sum(pseudovals) / len(pseudovals)
SE = sem(pseudovals)
Z = D / SE # Z is calculated with the uncorrected D value
print('OUT\tTEST\tX\tY\tD\tDcorrected\tSE\tZ\tN_Blocks\tN_ABBA\tN_BABA')
print(*(config.split(',')), D, Dcorr, SE, Z, len(glob), N_ABBA, N_BABA, sep = '\t')
